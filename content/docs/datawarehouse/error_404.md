---
title: Error 404
linkTitle: Error 404
weight: 10
---

RHEL data in DataWarehouse is protected behind SSO/LDAP. To get access, make
sure you are logged in to DataWarehouse via `Login with Red Hat SSO` button.

If you still don't have access to the data, make sure you are part of the
[correct LDAP groups].

[correct LDAP groups]: datawarehouse-ldap-groups.md#granting-internal-access
