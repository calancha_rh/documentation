---
title: "GitLab Pipeline"
description: >
  High-level description of the CKI GitLab pipeline
weight: 15
---

## Overview

CKI GitLab pipelines roughly come in two flavors: pipelines that build kernels
from source, and pipelines that download prebuilt kernels from Koji/Copr.

### Building kernels from source

The following figure provides a slightly simplified overview of a CKI GitLab
pipeline for a CentOS Stream 9 merge request where kernels are built from the
sources in the merge request:

![(Slightly simplified) source GitLab pipeline](pipeline-jobs-source.png)

Different colors represent different architectures of the underlying machines.
The machines are either provided by AWS EC2 ("AWS" with single outline on the
diagram) or by an on-premise data center ("DC" with double outline on the
diagram).

### Using prebuilt kernels

For comparison, this is a pipeline for a prebuilt kernel from Koji:

![(Slightly simplified) Koji GitLab pipeline](pipeline-jobs-koji.png)

Again, machines are either provided by AWS EC2 ("AWS" with single outline on
the diagram) or by an on-premise data center ("DC" with double outline on the
diagram).

## Pipeline jobs

While the GitLab pipelines run in the various branches of the [CKI pipeline
projects][pipeline-projects], the actual pipeline code comes from the
[pipeline-definition repository][pipeline-code].

Depending on various factors, the jobs in a given pipeline will be different:

- kernel built from source or already prebuilt via Koji
- supported architectures, native compilation or native tools
- retriggered pipelines for CI testing using artifacts from previous pipelines

## Retriggered pipelines

Retriggered pipelines use components of the [staging environment] where available.

[pipeline-projects]: https://gitlab.com/redhat/red-hat-ci-tools/kernel
[pipeline-code]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/cki_pipeline.yml
[staging environment]: ../staging/index.md
